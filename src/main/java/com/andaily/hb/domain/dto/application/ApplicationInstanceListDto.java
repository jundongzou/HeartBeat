package com.andaily.hb.domain.dto.application;

import com.andaily.hb.domain.shared.paginated.DefaultPaginated;

import java.util.Map;

/**
 * 15-1-3
 *
 * @author Shengzhao Li
 */
public class ApplicationInstanceListDto extends DefaultPaginated<ApplicationInstanceDto> {


    private String instanceName;

    private String email;

    //是否启用
    private String enabled;


    public ApplicationInstanceListDto() {
    }

    @Override
    public Map<String, Object> queryMap() {
        Map<String, Object> map = super.queryMap();
        map.put("instanceName", instanceName);
        map.put("enabled", enabled);
        map.put("email", email);
        return map;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

}
