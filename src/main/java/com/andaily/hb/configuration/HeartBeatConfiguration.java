package com.andaily.hb.configuration;

import com.andaily.hb.domain.shared.Application;
import com.andaily.hb.infrastructure.mail.MailTransmitter;
import com.andaily.hb.infrastructure.scheduler.DynamicSchedulerFactory;
import org.hibernate.SessionFactory;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;


/**
 * 2018/1/30
 * <p>
 * Replace  HeartBeat.xml
 *
 * @author Shengzhao Li
 */
@Configuration
@EnableTransactionManagement
//@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
public class HeartBeatConfiguration {


    @Value("${application.host}")
    private String host;


    @Value("${minixin.appkey}")
    private String minixinAppKey;


    @Value("${minixin.appsecret}")
    private String minixinAppSecret;


    @Value("${minixin.host}")
    private String minixinHost;

    @Value("${default.mail.address}")
    private String defaultMailAddress;

    @Value("${mail.develop.environment}")
    private boolean developEnvironment;

    @Value("${mail.send.use.thread}")
    private boolean sendMailUseThread;

    @Value("${scheduler.auto.startup}")
    private boolean schedulerAutoStartup;


    @Bean
    public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {
        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setPackagesToScan("com.andaily.hb.domain");

        Properties properties = new Properties();
        properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
        properties.setProperty("hibernate.show_sql", "false");
//        properties.setProperty("hibernate.hbm2ddl.auto", "none");
        properties.setProperty("jdbc.use_scrollable_resultset", "false");
        properties.setProperty("hibernate.query.substitutions", "true=1,false=0");
        properties.setProperty("hibernate.current_session_context_class", "org.springframework.orm.hibernate5.SpringSessionContext");
        factoryBean.setHibernateProperties(properties);

        return factoryBean;
    }


    @Bean
    public PlatformTransactionManager transactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory);
        return transactionManager;
    }


    @Bean
    public MailTransmitter mailTransmitter(JavaMailSender javaMailSender) {
        MailTransmitter transmitter = new MailTransmitter();
        transmitter.setDefaultMailSender(javaMailSender);
        transmitter.setDefaultFromAddress(defaultMailAddress);
        transmitter.setDevelopEnvironment(developEnvironment);
        transmitter.setSendMailUseThread(sendMailUseThread);
        return transmitter;
    }


    // Application
    @Bean
    public Application application() {
        Application application = new Application();
        application.setHost(host);
        application.setMinixinAppKey(minixinAppKey);
        application.setMinixinAppSecret(minixinAppSecret);
        application.setMinixinHost(minixinHost);
        return application;
    }


    @Bean
//    @Lazy
    public SchedulerFactoryBean scheduler(DataSource dataSource, PlatformTransactionManager transactionManager) throws Exception {
        SchedulerFactoryBean factoryBean = new SchedulerFactoryBean();

        factoryBean.setDataSource(dataSource);
        factoryBean.setTransactionManager(transactionManager);
        factoryBean.setAutoStartup(schedulerAutoStartup);

        return factoryBean;
    }


    /**
     * dynamic scheduler factory
     * 动态 定时任务 配置
     */
    @Bean
    public DynamicSchedulerFactory dynamicSchedulerFactory(Scheduler scheduler) {
        DynamicSchedulerFactory schedulerFactory = new DynamicSchedulerFactory();
        schedulerFactory.setScheduler(scheduler);
        return schedulerFactory;
    }


}
