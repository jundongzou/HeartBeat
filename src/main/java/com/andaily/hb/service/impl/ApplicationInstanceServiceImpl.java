package com.andaily.hb.service.impl;

import com.andaily.hb.domain.dto.application.ApplicationInstanceFormDto;
import com.andaily.hb.domain.dto.application.ApplicationInstanceListDto;
import com.andaily.hb.domain.dto.application.InstanceStatisticsDto;
import com.andaily.hb.service.ApplicationInstanceService;
import com.andaily.hb.service.operation.instance.*;
import com.andaily.hb.service.operation.job.PerHeartBeatExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 15-1-3
 *
 * @author Shengzhao Li
 */
@Service("applicationInstanceService")
public class ApplicationInstanceServiceImpl implements ApplicationInstanceService {


    @Transactional(readOnly = true)
    @Override
    public void loadApplicationInstanceListDto(ApplicationInstanceListDto listDto) {
        ApplicationInstanceListDtoLoader dtoLoader = new ApplicationInstanceListDtoLoader(listDto);
        dtoLoader.load();
    }


    @Transactional(readOnly = true)
    @Override
    public ApplicationInstanceFormDto loadApplicationInstanceFormDto(String guid) {
        ApplicationInstanceFormDtoLoader dtoLoader = new ApplicationInstanceFormDtoLoader(guid);
        return dtoLoader.load();
    }

    @Transactional
    @Override
    public void persistApplicationInstance(ApplicationInstanceFormDto formDto) {
        ApplicationInstanceFormDtoPersister persister = new ApplicationInstanceFormDtoPersister(formDto);
        persister.persist();
    }

    @Transactional
    @Override
    public boolean enableApplicationInstance(String guid) {
        ApplicationInstanceEnabler instanceEnabler = new ApplicationInstanceEnabler(guid);
        return instanceEnabler.enable();
    }

    @Transactional
    @Override
    public void executePerHeartBeatByInstanceGuid(String instanceGuid) {
        PerHeartBeatExecutor perHeartBeatExecutor = new PerHeartBeatExecutor(instanceGuid);
        perHeartBeatExecutor.execute();
    }

    @Transactional
    @Override
    public boolean stopMonitoringApplicationInstance(String guid) {
        MonitoringApplicationInstanceKiller instanceKiller = new MonitoringApplicationInstanceKiller(guid);
        return instanceKiller.kill();
    }

    @Transactional
    @Override
    public boolean deleteApplicationInstance(String guid) {
        ApplicationInstanceDeleter instanceDeleter = new ApplicationInstanceDeleter(guid);
        return instanceDeleter.delete();
    }


    @Transactional(readOnly = true)
    @Override
    public InstanceStatisticsDto loadInstanceStatisticsDto(String guid) {
        InstanceStatisticsDtoLoader statisticsDtoLoader = new InstanceStatisticsDtoLoader(guid);
        return statisticsDtoLoader.load();
    }
}
