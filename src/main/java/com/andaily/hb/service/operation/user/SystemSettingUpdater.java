package com.andaily.hb.service.operation.user;

import com.andaily.hb.domain.dto.user.SystemSettingDto;
import com.andaily.hb.domain.shared.Application;
import com.andaily.hb.domain.shared.BeanProvider;
import com.andaily.hb.domain.shared.security.SecurityUtils;
import com.andaily.hb.domain.user.SystemSetting;
import com.andaily.hb.domain.user.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 15-4-14
 * <p/>
 * 系统设置
 *
 * @author Shengzhao Li
 */
public class SystemSettingUpdater {

    private static final Logger LOGGER = LoggerFactory.getLogger(SystemSettingUpdater.class);

    private transient UserRepository userRepository = BeanProvider.getBean(UserRepository.class);
    private SystemSettingDto settingDto;

    public SystemSettingUpdater(SystemSettingDto settingDto) {
        this.settingDto = settingDto;
    }

    public void update() {
        final SystemSetting setting = userRepository.findSystemSetting();

        setting.allowUserRegister(settingDto.isAllowUserRegister());
        setting.cleanMonitorLogFrequency(settingDto.getCleanMonitorLogFrequency());
        //Be sure call saveOrUpdate
        userRepository.saveOrUpdate(setting);
        LOGGER.debug("<{}> update system setting: {}", SecurityUtils.currentUsername(), setting);

        //update global setting
        updateGlobalSetting(setting);
    }

    private void updateGlobalSetting(SystemSetting setting) {
        Application.systemSetting()
                .allowUserRegister(setting.allowUserRegister())
                .cleanMonitorLogFrequency(setting.cleanMonitorLogFrequency());
    }
}
