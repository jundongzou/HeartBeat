package com.andaily.hb.web.context;

import com.andaily.hb.domain.shared.BeanProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * 扩展 Spring Context 的容器初始化
 *
 * @author Shengzhao Li
 */
@Component
public class HBApplicationContextAware implements ApplicationContextAware {

    private static final Logger LOG = LoggerFactory.getLogger(HBApplicationContextAware.class);


    public HBApplicationContextAware() {
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        BeanProvider.initialize(applicationContext);
        LOG.info("Initialed BeanProvider from ApplicationContext: {}", applicationContext);
    }
}