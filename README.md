#HeartBeat
<div>
    <p>
     心跳检测各类应用服务器(如Tomcat,Jetty),WEB服务器(如 Apache,Nginx) 的JAVA WEB应用程序.
    </p>
    <p>
     如何实现?
     <br/>
     使用HttpClient对指定的服务器(application-instance) URL 按频率(10秒,20秒...) 发起请求并记录响应的信息(连接耗时,是否连接成功,是否有异常,响应数据包大小),
     若检测到不正常(响应码不是200,抛出异常...)时则发送邮件给指定的地址,当检测恢复正常时也发送提醒邮件.
     <br/>
     将来会添加更多的实时提醒方式接口,如微信,短信
    </p>
</div>

<h3>2.0.0及以上版本使用 Spring-Boot 框架重构, 向微服务靠近.</h3>

<div>
    <h3>使用的框架及版本</h3>
    <ul>
        <li>JDK - 1.8</li>
        <li>Spring Boot - 1.5.9.RELEASE</li>
        <li><a href="http://www.bootcss.com/p/flat-ui/">Flat UI</a></li>
        <li>Maven - 3.1.0</li>
    </ul>
</div>

<div>
    <h3>下载</h3>
    从0.3版本开始, 每一个版本的下载文件都在项目的 'dist' 目录.
    <ul>
        <li><a href="http://git.oschina.net/mkk/HeartBeat/raw/V-0.3/dist/HeartBeat-0.3.zip">HeartBeat-0.3.zip</a></li>
        <li><a href="http://git.oschina.net/mkk/HeartBeat/raw/V-0.4/dist/HeartBeat-0.4.zip">HeartBeat-0.4.zip</a></li>
        <li><a href="http://git.oschina.net/mkk/HeartBeat/raw/V-0.5/dist/HeartBeat-0.5.zip">HeartBeat-0.5.zip</a></li>
        <li><a href="https://gitee.com/mkk/HeartBeat/attach_files/download?i=90418&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F01%2FAD%2FPaAvDFmOZb-Ad23MAYZYy6d9bok867.zip%3Ftoken%3D2e73f9cd572d797d2c240b9ea107241e%26ts%3D1502504383%26attname%3DHeartBeat-1.0.0.zip">HeartBeat-1.0.0.zip</a></li>
        <li>最新版本下载: <a href="https://gitee.com/mkk/HeartBeat/raw/2.0.0/dist/HeartBeat-2.0.0.zip">HeartBeat-2.0.0.zip</a></li>
    </ul>
</div>

<div>
    <h3>特点</h3>
    <ul>
        <li>无侵入,独立部署</li>
        <li>可同时监测多个应用服务器</li>
        <li>请求方式支持GET,POST; URL支持http与https, 可指定请求content-type, 添加请求参数(固定参数或随机参数)</li>
        <li>添加安全设置,可控制用户注册,设定用户权限等</li>
        <li>使用简洁,灵活</li>
        <li>提醒方式及时,多样(目前已实现邮件提醒,微信提醒;将来会加入短信提醒等)</li>
    </ul>
</div>

<div>
    <h3>运行环境</h3>
    <ul>
        <li>JRE 1.8 +</li>
        <li>MySql 5.6 +</li>
        <li>Tomcat 8 +</li>
    </ul>
</div>

<div>
    <h3>在线测试</h3>
    <a href="http://andaily.com/hb/">http://andaily.com/hb/</a>
</div>

<div>
    <h3>如何使用?</h3>
    <ol>
        <li>项目是Maven管理的, 需要在电脑上安装maven(开发用的版本号为3.1.0), MySql(开发用的版本号为5.6)</li>
        <li>下载(或clone)项目到本地</li>
        <li>
            创建MySQL数据库(默认数据库名:heart_beat), 并运行相应的SQL脚本(脚本文件位于others/database目录),
            <br/>
            运行脚本的顺序: HeartBeat.ddl -> quartz_mysql_innodb.sql -> initial-db.ddl(该ddl会创建初始管理员账号/密码:hb/heart_beat)
        </li>
        <li>
            修改 application.properties(位于src/main/resources目录)中的数据库连接信息(包括username, password等)
            <br/>
            <strong>NOTE: 为了确保能收到提醒邮件,请将配置文件中的 <em>mail.develop.address</em> 配置为你的邮件地址;
            若在生产环境,请将 <em>mail.develop.environment</em> 值修改为 false (true表示为开发环境)</strong>
        </li>
        <li>
            将本地项目导入到IDE(如Intellij IDEA)中,配置Tomcat(或类似的servlet运行服务器), 并启动Tomcat(默认端口为8080)
            <br/>
               另: 也可通过maven package命令将项目编译为war文件(hb.war),
                     将war放在Tomcat中并启动(注意: 这种方式需要将 application.properties 加入到classpath中并正确配置数据库连接信息).
                     <br/>
                     或使用 java  -jar hb.war --spring.config.location=application.properties 命令运行(注意 application.properties 的路径要正确)
                     <br/>
                     或直接在项目的'dist'目录下载完整版安装包.
        </li>
        <li>
            HeartBeat运行后, 初次登录可使用默认管理员账号/密码:hb/heart_beat, 若失败, 需要在数据库中运行初始SQL脚本文件initial-db.ddl(others/database目录),
            账号密码使用MD5加密.
        </li>
    </ol>
    <h3>如何通过微信接收监控信息</h3>
    <ol>
        <li>通过微信扫描二维码 <img src="http://77g1is.com1.z0.glb.clouddn.com/qiuchang8_qrcode.jpg"/> 或添加公众号 qiuchang8;
        在关注后绑定在 <a href="http://andaily.com/hb/">http://andaily.com/hb/</a> 中注册的账号(一个账号可以绑定多个微信号)</li>
        <li>在添加或修改Instance 时勾选上需要接收监控信息的微信号并保存; 待有心跳检测不正常时发提示信息到微信</li>
    </ol>

</div>



<div>
    <h3>邮件配置说明</h3>
    <p>
        HeartBeat项目使用的邮件服务器使用SSL连接, 所以在配置邮件(javaMailSender, application.properties)时, 使用了SSL连接配置,包括<code>mail.smtp.auth</code>与<code>mail.smtp.socketFactory.class</code>;
        <br/>
        若在使用中配置邮件后不工作, 请检查配置(如使用的邮件服务器是否支持SSL)并编写单元测试来测试邮件发送能正常工作(项目的邮件单元测试在 MailTransmitterTest.java 文件中,
        记得将测试的emailAddress设置为自己邮箱).
        <br/>
        <strong>另:  强烈建议使用SSL连接邮件服务器</strong>
    </p>
    <p>
        在项目的配置文件<code>application.properties</code>中, 可配置邮件为开发环境或生产环境,具体参数为<code>mail.develop.environment</code>
        与<code>mail.develop.address</code>, 若将<code>mail.develop.environment</code> = true为生产环境, false为开发环境; 开发环境时的邮件只为
        发给<code>mail.develop.address</code>配置的邮箱,不会发给真正的邮件接收者; 生产环境时<code>mail.develop.address</code>配置不启作用.
    </p>
</div>


<hr/>
<h3>开发计划</h3>
<p>
从 0.5版本开始将项目的所有计划的开发内容列出来, 方便大家跟进, 也欢迎你加入.
<br/>
项目的开发管理使用开源项目 <a href="http://git.oschina.net/mkk/andaily-developer">andaily-developer</a>.
</p>
<ul>
       <li>
            <p>
                Version: <strong>2.0.0</strong> [finished]
                <br/>
                Date: 2018-01-29 / 2018-02-22
            </p>
            <ol>
                <li><p><del>使用 SprintBoot 重构项目</del></p></li>
            </ol>
            <br/>
       </li>
       <li>
            <p>
                Version: <strong>1.0.1</strong> [finished]
                <br/>
                Date: 2017-07-10 / 2018-01-20
            </p>
            <ol>
                <li><p><del>Fix issue #7 MenuBUGFix</del></p></li>
                <li><p><del>实例列表增加更多搜索条件</del></p></li>
                <li><p>#20 加一个根据http返回结果来决定请求状态(CANCELED)</p></li>
                <li><p>添加Socket TCP/IP的监控(CANCELED)</p></li>
            </ol>
            <br/>
       </li>
       <li>
            <p>
                Version: <strong>1.0.0</strong> [finished]
                <br/>
                Date: 2016-11-18 / 2017-07-09
            </p>
            <ol>
                <li><p>(104) - 添加发送短信提醒(CANCELED)</p></li>
                <li><p><del>(165) - 添加更多监控时间,如2分钟,3分钟,5分钟,10分钟,半小时,1小时 </del></p></li>
                <li><p><del>(166) - Fix 设置多次失败后在提醒时的错误,以及恢复后的提醒 </del></p></li>
                <li><p><del>(168) - 增加设置定期清理一段时间(默认为30天)前的监控日志记录,防止frequency_monitor_log表太大导致查询慢 </del></p></li>
                <li><p><del>修复未登录时不能查看到私有实例的日志</del></p></li>
                <li><p><del>Fixed Issue #26 关于邮件发送次数的问题</del></p></li>
            </ol>
            <br/>
       </li>
       <li>
            <p>
                Version: <strong>0.5</strong> [finished]
                <br/>
                Date: 2016-05-01 / 2016-08-15
            </p>
            <ol>
                <li><p><del>(70) - Why set archived = 1 in mysql application_instance table(Fix issue #6)</del></p></li>
                <li><p><del>(83) - 对于注册的用户, 登录后只能管理自己 创建的instances</del></p></li>
                <li><p><del>(112) - #12 请求参数BUG(设置Url参数时未进行非空验证)</del></p></li>
                <li><p>(104) - 添加发送短信提醒(CANCELED)</p></li>
                <li><p><del>(105) - 添加发送微信提醒</del></p></li>
                <li><p><del>(147) - 支持国际化, 中英文</del></p></li>
                <li><p><del>(148) - 在检测链接状态的时候，不要发现一次链接故障的时候就马上发邮件通知，这个过程可能因为网络故障（如常见的：丢包、dns故障）而出现问题，
                可以增加一个判断参数，当连续发现n次有故障时再去发邮箱等信息通知！</del></p></li>
                <li><p><del>(156) - 优化数据库,以及连接, 提高性能,页面响应速度</del></p></li>
                <li><p><del>(157) - 实例添加一个字段 是否私有, 若私有则只有自己登录后才能查看</del></p></li>
            </ol>
       </li>

</ul>
<br/>


<div>
    <h3>Change-Log</h3>
    <ol>
        <li>
            <p>
                2014-10-17   ----    Initial project
            </p>
        </li>
        <li>
            <p>
                2015-02-13   ----    Move development to <a href="https://coding.net/u/monkeyk/p/HeartBeat/git">coding.net</a>
            </p>
        </li>
        <li>
            <p>
                2015-03-01   ----    Back to OSC and update documents; Add 0.1 branch
            </p>
        </li>
        <li>
            <p>
                2015-03-14   ----    Monitoring log add response data size;Add list of monitoring reminder logs; Update page styles; Add 0.2 branch
            </p>
        </li>
        <li>
            <p>
                2015-03-15   ----    0.3 branch is developing
            </p>
        </li>
        <li>
            <p>
                2015-04-02   ----    Add 0.3 branch and publish it
            </p>
        </li>
        <li>
            <p>
                2015-04-06   ----    0.4 branch is developing
            </p>
        </li>
        <li>
            <p>
                2015-05-01   ----    Publish 0.4 version
            </p>
        </li>
        <li>
            <p>
                2016-01-09   ----    Deploy war to Test-Server(http://andaily.com/hb/)
            </p>
        </li>
        <li>
            <p>
                2016-05-01   ----    Start to developing 0.5 version
            </p>
        </li>
        <li>
            <p>
                2016-07-30   ----    Publish 0.5-beta1 version
            </p>
        </li>
        <li>
            <p>
                2016-08-15   ----    Publish 0.5-beta2 version
            </p>
        </li>
        <li>
            <p>
                2016-11-18   ----    Publish 0.5 version and add 1.0.0 planning
            </p>
        </li>
        <li>
            <p>
                2017-07-09   ----    Publish 1.0.0 version
            </p>
        </li>        
        <li>
            <p>
                2018-01-29   ----    Start Spring-Boot 2.0.0 development
            </p>
        </li>        
        <li>
            <p>
                2018-02-22   ----    Publish 2.0.0 version
            </p>
        </li>
    </ol>
</div>


<div>
    <h3>程序运行主要截图</h3>
    <small>小提示: 截图不是最新版本, 可访问在线测试 <a href="http://andaily.com/hb/">http://andaily.com/hb/</a> 查看最新功能</small>
    <ol>
        <li>
            <p>
                Monitoring
                <br/>
                <img src="http://andaily.qiniudn.com/hbmonitoring_0.3.png" alt="hb"/>
                <br/>
            </p>
        </li>
        <li>
            <p>
                Instance - Monitoring details
                <br/>
                <img src="http://andaily.qiniudn.com/hbmonitoring-details_0.3.png" alt="hb"/>
                <br/>
            </p>
        </li>
        <li>
            <p>
                Instance - Overview
                <br/>
                <img src="http://andaily.qiniudn.com/hbinstances_0.3.png" alt="hb"/>
                <br/>
            </p>
        </li>
        <li>
            <p>
                Instance - Create
                <br/>
                <img src="http://andaily.qiniudn.com/hbnew-instance_0.3.png" alt="hb"/>
                <br/>
            </p>
        </li>
        <li>
            <p>
                Monitoring-Log
                <br/>
                <img src="http://andaily.qiniudn.com/hbhb-log_0.3.png" alt="hb"/>
                <br/>
            </p>
        </li>
        <li>
            <p>
                Monitoring-Reminder-Log
                <br/>
                <img src="http://andaily.qiniudn.com/hbreminder-log_0.3.png" alt="hb"/>
                <br/>
            </p>
        </li>
        <li>
            <p>
                Search
                <br/>
                <img src="http://andaily.qiniudn.com/hbsearch_0.3.png" alt="hb"/>
                <br/>
            </p>
        </li>
    </ol>
</div>


<hr/>
<div>
    <h3>相关链接</h3>
    <ul>
        <li><a href="http://www.oschina.net/p/java-heartbeat">应用服务器心跳检测 Java HeartBeat</a></li>
        <li><a href="http://blog.csdn.net/monkeyking1987/article/details/44004231">心跳检测服务器是否正常的开源项目</a></li>
        <li><a href="http://www.oschina.net/news/69661/2015-git-osc--opensource-project-ranking-top-50">2015 年 Git@OSC 最热门开源项目 Top 50</a>, HeartBeat荣登其中</li>
    </ul>
</div>

<hr/>
<p>
    <strong>捐助</strong>
    <br/>
    支付宝: monkeyking1987@126.com (**钊)
    <ul>
        <li><p>曹晓舟 -- 18元 (2017-02-19)</p></li>
    </ul>
</p>

<hr/>
<div>
    More Open-Source projects see <a href="http://andaily.com/my_projects.html">http://andaily.com/my_projects.html</a>
    <br/>
    From <a href="http://andaily.com">andaily.com</a>
    <br/>
    Email <a href="mailto:monkeyk@shengzhaoli.com">monkeyk@shengzhaoli.com</a>
</div>
<p>
    <img src="http://77g1is.com1.z0.glb.clouddn.com/wechat_qrcode.jpg"/>
</p>